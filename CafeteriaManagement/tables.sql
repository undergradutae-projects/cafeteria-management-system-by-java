drop table student;
create table student(
	id numeric primary key,
	name varchar2(30),
	dept varchar2(5),
	hall varchar2(10)
);

insert into student values (124401,'Redwan Karim','CSE','North');
insert into student values (124402,'Salman Khan','CSE','North');
insert into student values (124403,'Mashroor Nayar','CSE','North');
insert into student values (124404,'Shawon Ashraf','CSE','North');
insert into student values (124405,'Abrar Fayaz','CSE','North');
insert into student values (124406,'Rifat Ahmed','CSE','North');
insert into student values (124407,'Nafis Neehal','CSE','North');
insert into student values (124408,'Asad Manik','CSE','North');
insert into student values (124409,'Kayes Ahmed','CSE','South');
insert into student values (124410,'Benazir Ahmed','CSE','South');
insert into student values (124411,'Mahbubul Alam','CSE','South');


create table staff(
	id numeric primary key,
	name varchar2(30),
	
);

create table student_login(
    sid numeric ,
    password varchar2(20),
    foreign key(sid) references student
);

insert into student_login values (124401,'s124401');
insert into student_login values (124402,'s124402');
insert into student_login values (124403,'s124403');
insert into student_login values (124404,'s124404');
insert into student_login values (124405,'s124405');
insert into student_login values (124406,'s124406');
insert into student_login values (124407,'s124407');
insert into student_login values (124408,'s124408');
insert into student_login values (124409,'s124409');
insert into student_login values (124410,'s124410');

create table staff(
    s_id numeric primary key,
    s_name varchar(15),
    s_pw varchar(10),
    sat_sun int,
    mon_tue int,
    wed_thu int,
    fri int
);


insert into staff values(2001,'Selim Molla','test2001',0,0,0,0);
insert into staff values(2002,'Abu Molla','test2002',0,0,0,0);
insert into staff values(2003,'Kalam Molla','test2003',0,0,0,0);
insert into staff values(2004,'Jamal Molla','test2004',0,0,0,0);
insert into staff values(2005,'Rahim Molla','test2005',0,0,0,0);
insert into staff values(2006,'Mohin Molla','test2001',0,0,0,0);
insert into staff values(2007,'Nafis Molla','test2002',0,0,0,0);
insert into staff values(2008,'Ajij Molla','test2003',0,0,0,0);
insert into staff values(2009,'Ananta Molla','test2004',0,0,0,0);
insert into staff values(2010,'Sakib Molla','test2005',0,0,0,0);
insert into staff values(2011,'Shahrukh Molla','test2001',0,0,0,0);
insert into staff values(2012,'Hrittik Molla','test2002',0,0,0,0);
insert into staff values(2013,'Abraham Molla','test2003',0,0,0,0);
insert into staff values(2014,'Amir Molla','test2004',0,0,0,0);
insert into staff values(2015,'Ranbir Molla','test2005',0,0,0,0);

drop table food;
create table food(
f_id numeric primary key,
f_name varchar(25),
category varchar(10),
vote numeric,
day varchar(10)
); 

insert into food values(1,'Tanduri','Launch',0,'Sat');
insert into food values(2,'Sweet Beef Curry','Launch',0,'Sat');
insert into food values(3,'Khichuri','Launch',0,'Sat');
insert into food values(4,'Sweet Chicken Roast','Dinner',0,'Sat');
insert into food values(5,'Beef Curry','Dinner',0,'Sat');
insert into food values(6,'Fish Curry','Dinner',0,'Sat');
insert into food values(7,'Tanduri','Launch',0,'Sun');
insert into food values(8,'Dry Fish','Launch',0,'Sun');
insert into food values(9,'Beef Keema ','Launch',0,'Sun');
insert into food values(10,'Noddles','Dinner',0,'Sun');
insert into food values(11,'Tanduri','Dinner',0,'Sun');
insert into food values(12,'Fish Curry','Dinner',0,'Sun');
insert into food values(13,'Tanduri','Launch',0,'Mon');
insert into food values(14,'Chicken Chilli Curry','Launch',0,'Mon');
insert into food values(15,'Beef Curry ','Launch',0,'Mon');
insert into food values(16,'Fish Curry','Dinner',0,'Mon');
insert into food values(17,'Mutton Curry','Dinner',0,'Mon');
insert into food values(18,'Broiler Chicken Curry','Dinner',0,'Mon');
insert into food values(19,'Tanduri','Launch',0,'Tues');
insert into food values(20,'Mutton Curry','Launch',0,'Tues');
insert into food values(21,'Beef Keema ','Launch',0,'Tues');
insert into food values(22,'Magluba','Dinner',0,'Tues');
insert into food values(23,'Chicken Roast','Dinner',0,'Tues' );
insert into food values(24,'Egg Curry','Dinner',0,'Tues');
insert into food values(25,'Magluba','Launch',0,'Wed');
insert into food values(26,'Vegetables mash','Launch',0,'Wed');
insert into food values(27,'Beef Curry ','Launch',0,'Wed');
insert into food values(28,'Chicken Fried','Dinner',0,'Wed');
insert into food values(29,'Beef Curry','Dinner',0,'Wed');
insert into food values(30,'Chinese Vegetables','Dinner',0,'Wed') ;
insert into food values(31,'Fish Curry','Launch',0,'Thus');
insert into food values(32,'Kachhi Biriani','Launch',0,'Thus');
insert into food values(33,'Broleir Chicken Curry ','Launch',0,'Thus');
insert into food values(34,'Tanduri','Dinner',0,'Thus');
insert into food values(35,'Broiler Chicken Korma','Dinner',0,'Thus');
insert into food values(36,'Mutton Curry','Dinner',0,'Thus');
insert into food values(37,'Chicken Roast','Launch',0,'fri');
insert into food values(38,'Kachhi Biriani','Launch',0,'fri');
insert into food values(39,'Fish Curry ','Launch',0,'fri');
insert into food values(40,'Tanduri','Dinner',0,'fri');
insert into food values(41,'Meat Ball','Dinner',0,'fri');
insert into food values( 42,'Beef Curry','Dinner',0,'fri');


create table meal (
sId numeric primary key,
sName varchar2(20),
breakfast int,
launch int,
dinner int
);

create table cafelog (
id numeric,
name varchar(20),
password varchar2(5),
foreign key(id) references student
);

insert into cafelog values(124401,'Redwan Karim','4401');
insert into cafelog values(124402,'Salman Khan','4402');
insert into cafelog values(124403,'Mashroor Nayar','4403');
insert into cafelog values(124404,'Shawon Ashraf','4404');
insert into cafelog values(124405,'Abrar Fayaz','4405');
insert into cafelog values(124406,'Rifat Ahmed','4406');
insert into cafelog values(124407,'Nafis Neehal','4407');
insert into cafelog values(124408,'Asad Manik','4408');
insert into cafelog values(124409,'Kayes Ahmed','4409');
insert into cafelog values(124410,'Benazir Ahmed','4410');
insert into cafelog values(124411,'Mahbubul Alam','4411');


insert into meal values(124401,'Redwan Karim',0,0,0);
insert into meal values(124402,'Salman Khan',0,0,0);
insert into meal values(124403,'Mashroor Nayar',0,0,0);
insert into meal values(124404,'Shawon Ashraf',0,0,0);
insert into meal values(124405,'Abrar Fayaz',0,0,0);
insert into meal values(124406,'Rifat Ahmed',0,0,0);
insert into meal values(124407,'Nafis Neehal',0,0,0);
insert into meal values(124408,'Asad Manik',0,0,0);
insert into meal values(124409,'Kayes Ahmed',0,0,0);
insert into meal values(124410,'Benazir Ahmed',0,0,0);

