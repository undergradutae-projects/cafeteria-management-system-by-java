/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cafe;

import static java.lang.Thread.sleep;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * admin_form class is for the admin here the admin enters with his password he
 * can check the student info can check the mood menu can make the schedule of
 * the staff
 *
 * @author shovan
 */
public class Admin_form extends javax.swing.JFrame {

    /**
     * Creates new form Admin_form
     */
    public Admin_form() {
        initComponents();
        CurrentDate();
        comboList();
    }

    int current_week = 0;
    int next_week = 0;
    int day1;
    int hou;
    int monday = 0;

    public void CurrentDate() {

        Thread clock = new Thread() {
            public void run() {
                for (;;) {
                    Calendar cal = new GregorianCalendar();
                    int mon = cal.get(Calendar.MONTH);
                    int yr = cal.get(Calendar.YEAR);
                    day1 = cal.get(Calendar.DAY_OF_MONTH);
                    hou = cal.get(Calendar.HOUR_OF_DAY);
                    monday = cal.get(Calendar.DAY_OF_WEEK);
                    month = mon + 1;
                    year = yr;
                    m.setText("" + month);
                    y.setText("" + year);
                    if (monday == 2) {
                        mond.setText("* Manage the Schedule of the Staffs");
                    } else {
                        mond.setText("* No major task today");
                    }
                    if (day1 == 1) {
                        men.setText("* Check The Food Menu");
                    } else {
                        men.setText("* Food Menu is Selected");
                    }
                    fmonth = mon + 1;
                    frommonth.setText("" + fmonth);
                    fyear = yr;
                    fromyear.setText("" + fyear);
                    tmonth = mon + 1;
                    tomonth.setText("" + tmonth);
                    tyear = yr;
                    toyear.setText("" + tyear);
                    current_week = cal.get(Calendar.WEEK_OF_MONTH);
                    next_week = current_week + 1;

                    cweek.setText("" + current_week);
                    cmon.setText("of Month No. :" + (mon + 1));

                    date.setText("Date  " + day1 + "/" + (mon + 1) + "/" + yr);

                    int sec = cal.get(Calendar.SECOND);
                    int min = cal.get(Calendar.MINUTE);
                    int hr = cal.get(Calendar.HOUR);

                    time.setText("Time  " + hr + ":" + min + ":" + sec);
                    DATA_REFRESH1();
                    try {
                        sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LogInForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        clock.start();

    }

    int month;
    int year;

    /**
     * DATA_REFRESH1 just refresh the menu+schedule on the 30th day of the month
     *
     */
    private void DATA_REFRESH1() {
        int i = 0;
        if(monday == 1){
            try {
                
                PreparedStatement psa;
                psa = DB_connect.connection.prepareStatement("update again_sched set SATSUN = ?, MONTUE =?, WEDTHU=?,FRI=?");
                psa.setLong(1, (long) i);
                psa.setLong(2, (long) i);
                psa.setLong(3, (long) i);
                psa.setLong(4, (long) i);
                //ps.setLong(2, (long) sid);
                psa.executeUpdate();

                PreparedStatement ps;
                ps = DB_connect.connection.prepareStatement("update staff set SAT_SUN = ?");
                ps.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps.executeUpdate();

                PreparedStatement ps1;

                ps1 = DB_connect.connection.prepareStatement("update staff set MON_TUE = ?");
                ps1.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps1.executeUpdate();

                PreparedStatement ps2;

                ps2 = DB_connect.connection.prepareStatement("update staff set WED_THU = ?");
                ps2.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps2.executeUpdate();

                PreparedStatement ps3;
                ps3 = DB_connect.connection.prepareStatement("update staff set FRI = ?");
                ps3.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps3.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                Logger.getLogger(Password.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (day1 == 30 && hou == 23) {

            //System.out.println("AA");
            try {
                PreparedStatement ps12;
                ps12 = DB_connect.connection.prepareStatement("update again_meal set sat = ? , sun = ? , mon = ? , tues = ? , wed = ? , thurs = ? , fri = ?  ");
                ps12.setLong(1, (long) i);
                ps12.setLong(2, (long) i);
                ps12.setLong(3, (long) i);
                ps12.setLong(4, (long) i);
                ps12.setLong(5, (long) i);
                ps12.setLong(6, (long) i);
                ps12.setLong(7, (long) i);

                //ps.setLong(2, (long) sid);
                ps12.executeUpdate();

                PreparedStatement ps11;
                ps11 = DB_connect.connection.prepareStatement("update present_student set p_date = ? , p_month = ? , p_year = ? , from_date = ? , from_month = ? , from_year = ? , to_date = ? , to_month = ? , to_year = ? ");
                ps11.setLong(1, (long) i);
                ps11.setLong(2, (long) i);
                ps11.setLong(3, (long) i);
                ps11.setLong(4, (long) i);
                ps11.setLong(5, (long) i);
                ps11.setLong(6, (long) i);
                ps11.setLong(7, (long) i);
                ps11.setLong(8, (long) i);
                ps11.setLong(9, (long) i);
                //ps.setLong(2, (long) sid);
                ps11.executeUpdate();

                PreparedStatement ps0;
                ps0 = DB_connect.connection.prepareStatement("update food set VOTE = ?");
                ps0.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps0.executeUpdate();

                PreparedStatement ps;
                ps = DB_connect.connection.prepareStatement("update staff set SAT_SUN = ?");
                ps.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps.executeUpdate();

                PreparedStatement ps1;

                ps1 = DB_connect.connection.prepareStatement("update staff set MON_TUE = ?");
                ps1.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps1.executeUpdate();

                PreparedStatement ps2;

                ps2 = DB_connect.connection.prepareStatement("update staff set WED_THU = ?");
                ps2.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps2.executeUpdate();

                PreparedStatement ps3;
                ps3 = DB_connect.connection.prepareStatement("update staff set FRI = ?");
                ps3.setLong(1, (long) i);
                //ps.setLong(2, (long) sid);
                ps3.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                Logger.getLogger(Password.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    /**
     * adds values of the used combo boxes such the info of staffs and students
     */
    private void comboList() {

        try {

            Statement state = DB_connect.connection.createStatement();

            String query = "Select * from staff";
            ResultSet rs = state.executeQuery(query);
            while (rs.next()) {

                String i = rs.getString("s_name");
                int j = (int) rs.getInt("sat_sun");
                int k = (int) rs.getInt("mon_tue");
                int l = (int) rs.getInt("wed_thu");
                int f = (int) rs.getInt("fri");

                if (j == 0 && k == 0 && l == 0 && f == 0) {
                    sc1.removeItem(i);
                    sc2.removeItem(i);
                    sc3.removeItem(i);
                    mc1.removeItem(i);
                    mc2.removeItem(i);
                    mc3.removeItem(i);
                    wc1.removeItem(i);
                    wc2.removeItem(i);
                    wc3.removeItem(i);
                    fc1.removeItem(i);
                    fc2.removeItem(i);
                    fc3.removeItem(i);
                    sc1.addItem(i);
                    sc2.addItem(i);
                    sc3.addItem(i);
                    mc1.addItem(i);
                    mc2.addItem(i);
                    mc3.addItem(i);
                    wc1.addItem(i);
                    wc2.addItem(i);
                    wc3.addItem(i);
                    fc1.addItem(i);
                    fc2.addItem(i);
                    fc3.addItem(i);

                }

            }
            rs.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     *
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        show_stdnt_data = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        stuhall = new javax.swing.JComboBox();
        hall_wise = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        north_stu = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab1 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        total_stu = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        south_stu = new javax.swing.JTextField();
        clr_all_stu_rec = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel14 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        cmon = new javax.swing.JLabel();
        c2 = new javax.swing.JButton();
        day = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        free = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tab3 = new javax.swing.JTable();
        cweek = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        show = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tab2 = new javax.swing.JTable();
        c = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        sc3 = new javax.swing.JComboBox();
        sc2 = new javax.swing.JComboBox();
        sc1 = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        sub1 = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        mc3 = new javax.swing.JComboBox();
        mc1 = new javax.swing.JComboBox();
        mc2 = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        sub2 = new javax.swing.JButton();
        jPanel18 = new javax.swing.JPanel();
        wc3 = new javax.swing.JComboBox();
        wc1 = new javax.swing.JComboBox();
        wc2 = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        sub3 = new javax.swing.JButton();
        jPanel19 = new javax.swing.JPanel();
        fc3 = new javax.swing.JComboBox();
        fc1 = new javax.swing.JComboBox();
        fc2 = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        sub4 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        s_f = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        allfood = new javax.swing.JTable();
        refresh_all_food = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        sday = new javax.swing.JComboBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabs = new javax.swing.JTable();
        jButton4 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        ref = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        s1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        d = new javax.swing.JComboBox();
        m = new javax.swing.JTextField();
        y = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        clear1 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        todate = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        toyear = new javax.swing.JTextField();
        fromyear = new javax.swing.JTextField();
        frommonth = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        fromdate = new javax.swing.JComboBox();
        tomonth = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        s2 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        t_s = new javax.swing.JTextField();
        clear = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        stab = new javax.swing.JTable();
        jPanel21 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        mond = new javax.swing.JLabel();
        men = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        logOff = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Admin Menu");
        setAlwaysOnTop(true);

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jPanel4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        show_stdnt_data.setText("Show All Students Data");
        show_stdnt_data.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_stdnt_dataActionPerformed(evt);
            }
        });

        jPanel20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        stuhall.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "South", "North" }));

        hall_wise.setText("Show Hall wise Students Data");
        hall_wise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hall_wiseActionPerformed(evt);
            }
        });

        jLabel1.setText("Select Hall    :");

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stuhall, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(hall_wise))
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(stuhall, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(hall_wise)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(show_stdnt_data, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(show_stdnt_data))
                .addGap(30, 30, 30))
        );

        jPanel5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel3.setText("Total Students of South Hall :");

        tab1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "Dept", "Hall"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tab1);

        jLabel4.setText("Total Students of North Hall :");

        jLabel2.setText("Total Students Number            :");

        south_stu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                south_stuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 535, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(71, 71, 71)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(total_stu, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(south_stu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(north_stu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel2, jLabel3, jLabel4});

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {north_stu, south_stu, total_stu});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(total_stu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(south_stu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(north_stu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addContainerGap())
        );

        clr_all_stu_rec.setText("Refresh");
        clr_all_stu_rec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clr_all_stu_recActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(clr_all_stu_rec)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(clr_all_stu_rec)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(118, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Students Record", jPanel1);

        jPanel6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        c2.setText("Refresh");
        c2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c2ActionPerformed(evt);
            }
        });

        day.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Saturday & Sunday", "Monday & Tuesday", "Wednesday & Thursday", "Friday" }));

        jButton1.setText("Show Selected Staff");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        free.setText("Free Staff");

        jLabel6.setText("Current Week No. :");

        tab3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tab3);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cweek, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(day, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(free)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmon, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 519, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(c2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6)
                            .addComponent(cmon, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(day, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(free)
                            .addComponent(jButton1)))
                    .addComponent(cweek, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(c2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jPanel7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        show.setText("Show All Staff Data");
        show.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showActionPerformed(evt);
            }
        });

        tab2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(tab2);

        c.setText("Refresh");
        c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(show)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(c)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(show)
                    .addComponent(c))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel14Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPanel6, jPanel7});

        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Staff Record", jPanel14);

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder("Saturday & Sunday"));

        jLabel20.setText("Staff 1");

        jLabel21.setText("Staff 2");

        jLabel22.setText("Staff 3");

        sub1.setText("Submit");
        sub1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sub1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addGap(2, 2, 2)
                        .addComponent(sc1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                        .addComponent(sc2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sc3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(sub1)))
                .addGap(21, 21, 21))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(sc1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(sc2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(sc3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sub1)
                .addContainerGap())
        );

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder("Monday & Tuesday"));

        jLabel16.setText("Staff 1");

        jLabel23.setText("Staff 2");

        jLabel24.setText("Staff 3");

        sub2.setText("Submit");
        sub2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sub2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(3, 3, 3)
                        .addComponent(mc1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mc2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                        .addComponent(mc3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(sub2)))
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mc1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mc2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mc3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel24)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sub2)
                .addContainerGap())
        );

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder("Wednesday & Thursday"));

        jLabel17.setText("Staff 1");

        jLabel25.setText("Staff 2");

        jLabel26.setText("Staff 3");

        sub3.setText("Submit");
        sub3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sub3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel17)
                        .addGap(4, 4, 4)
                        .addComponent(wc1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(wc2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(wc3, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 21, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sub3)))
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(wc1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(wc2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel26)
                    .addComponent(wc3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sub3))
        );

        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder("Friday"));

        fc3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fc3ActionPerformed(evt);
            }
        });

        fc2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fc2ActionPerformed(evt);
            }
        });

        jLabel18.setText("Staff 1");

        jLabel19.setText("Staff 2");

        jLabel27.setText("Staff 3");

        sub4.setText("Submit");
        sub4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sub4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(4, 4, 4)
                        .addComponent(fc1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel19)
                        .addGap(4, 4, 4)
                        .addComponent(fc2, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fc3, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(sub4)))
                .addContainerGap())
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fc1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fc2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fc3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel27)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sub4))
        );

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel15Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPanel16, jPanel17, jPanel18, jPanel19});

        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(76, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Staff Scheduling", jPanel15);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jTabbedPane2)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Staff Management", jPanel3);

        jPanel9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        s_f.setText("All Available Food Items");
        s_f.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                s_fActionPerformed(evt);
            }
        });

        allfood.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Day", "Food", "Category"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(allfood);

        refresh_all_food.setText("Refresh");
        refresh_all_food.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refresh_all_foodActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(s_f)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(refresh_all_food))
                    .addComponent(jScrollPane4))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(s_f)
                    .addComponent(refresh_all_food))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Most Selected Items", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Abyssinica SIL", 1, 17))); // NOI18N

        sday.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));

        tabs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Lunch", "Dinner"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tabs);

        jButton4.setText("Most Selected Items");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel5.setText("Day      :");

        ref.setText("Refresh");
        ref.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 35, Short.MAX_VALUE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(75, 75, 75)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ref)))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jButton4)
                    .addComponent(ref))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(155, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Food Menu ", jPanel8);

        jPanel12.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        s1.setText("Submit Request");
        s1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                s1ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel7.setText("Select Specific Date   :");

        d.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        jLabel28.setText("Day");

        jLabel29.setText("Month");

        jLabel30.setText("Year");

        clear1.setText("Refresh");
        clear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clear1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(d, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(m))
                .addGap(34, 34, 34)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addContainerGap())
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(y, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(s1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clear1))))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(d, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(m, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(y, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(s1)
                            .addComponent(clear1)))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel28)
                            .addComponent(jLabel29)
                            .addComponent(jLabel30))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                        .addComponent(jLabel7)))
                .addContainerGap())
        );

        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NAME", "FROM", "TO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tab);

        jLabel8.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel8.setText("Absent Student's Info :   ");

        jPanel13.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel13.setText("Year");

        todate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        jLabel11.setText("Date");

        jLabel9.setText("From     :");

        fromdate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        jLabel10.setText("To           :");

        jLabel12.setText("Month");

        s2.setText("Submit Request");
        s2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                s2ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel14.setText("Select Range Of Date(s)    :");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addGap(47, 47, 47)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fromdate, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(todate, 0, 72, Short.MAX_VALUE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(frommonth, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tomonth, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fromyear, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(toyear, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(86, 86, 86))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(s2)
                .addContainerGap())
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {fromdate, todate});

        jPanel13Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {frommonth, tomonth});

        jPanel13Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {fromyear, toyear});

        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13))
                .addGap(7, 7, 7)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fromdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(frommonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fromyear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(todate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tomonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(toyear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(s2)
                .addGap(41, 41, 41))
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {fromdate, todate});

        jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {frommonth, tomonth});

        jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {fromyear, toyear});

        jLabel15.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel15.setText("Total Absent Students    :");

        clear.setText("Refresh");
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearActionPerformed(evt);
            }
        });

        stab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(stab);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3)
                    .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addGap(18, 18, 18)
                        .addComponent(t_s, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 318, Short.MAX_VALUE))
                    .addComponent(jPanel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clear)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(clear))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(t_s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addGap(81, 81, 81))
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel15, t_s});

        jTabbedPane1.addTab("Student Report", jPanel11);

        jPanel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        date.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N

        time.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(time, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(date, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
                    .addComponent(time, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Notifications", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Liberation Serif", 1, 14), java.awt.Color.red)); // NOI18N

        mond.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mond.setForeground(new java.awt.Color(57, 191, 93));

        men.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        men.setForeground(new java.awt.Color(57, 191, 93));

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(men, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mond, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mond, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(men, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jMenu1.setText("File");

        logOff.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        logOff.setText("Log Off");
        logOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logOffActionPerformed(evt);
            }
        });
        jMenu1.add(logOff);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(22, Short.MAX_VALUE)
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 673, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, 272, Short.MAX_VALUE)))
                .addGap(18, 18, 18))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * this method is for clearing the used tables' data
     *
     */
    public void tab_clr() {
        DefaultTableModel model = (DefaultTableModel) tab1.getModel();
        String sname = "";
        String sid = "";
        String sdept = "";
        String shall = "";
        model.addRow(new Object[]{sname, sid, sdept, shall});
    }

    /**
     * for logging off
     *
     */
    private void logOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logOffActionPerformed
        // TODO add your handling code here:
        new Caution().setVisible(true);
//        new LogInForm().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_logOffActionPerformed

    /**
     * for selecting food
     *
     * @param evt
     */
    private void s_fActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_s_fActionPerformed
        // TODO add your handling code here:
        clr_all_food();
        DefaultTableModel model = (DefaultTableModel) allfood.getModel();
        try {

            Statement state = DB_connect.connection.createStatement();
            String query = "Select * from food";
            ResultSet rs = state.executeQuery(query);
            while (rs.next()) {
                String lun = null;
                String din = null;
                String day = rs.getString("day");
                String cat = rs.getString("category");
                lun = rs.getString("f_name");

                if ("Mon".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Monday", lun, cat});
                    count_all_food++;
                } else if ("Tues".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Tuesday", lun, cat});
                    count_all_food++;
                } else if ("Wed".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Wednesday", lun, cat});
                    count_all_food++;
                } else if ("Thus".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Thursday", lun, cat});
                    count_all_food++;
                } else if ("fri".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Friday", lun, cat});
                    count_all_food++;
                } else if ("Sat".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Saturday", lun, cat});
                    count_all_food++;
                } else if ("Sun".equals(day)) {
                    //model.addRow(new Object[]{day,lun,din});
                    model.addRow(new Object[]{"Sunday", lun, cat});
                    count_all_food++;
                }

            }

            rs.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }

    }//GEN-LAST:event_s_fActionPerformed

    /**
     * for searching hall wise student info
     *
     * @param evt
     */
    private void hall_wiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hall_wiseActionPerformed
        // TODO add your handling code here:
        //DefaultTableModel model = (DefaultTableModel) tab1.getModel();
        select = 2;
        clr_all_stu_rec(2);
        DefaultTableModel model = (DefaultTableModel) tab1.getModel();
        try {

            Statement state = DB_connect.connection.createStatement();
            //String hl =String.valueOf(stuhall.getSelectedItem());
            //String hl =String.valueOf(shl.getText());
            String hl = String.valueOf(stuhall.getSelectedItem());
            System.out.print(hl);
            String query = "Select * from student";
            ResultSet rs = state.executeQuery(query);
            int flag = 0;
            while (rs.next()) {

                String sname = rs.getString("name");
                String sid = rs.getString("id");
                String sdept = rs.getString("dept");
                String shall = rs.getString("hall");

                if (shall.equals(hl)) {
                    flag = 1;
                    model.addRow(new Object[]{sname, sid, sdept, shall});
                    count_par_hall++;
                    total_student++;

                    if (shall.equals("South")) {
                        south_student++;
                    } else if (shall.equals("North")) {
                        north_student++;
                    }
                    //south_student++;
                }

            }
            if (flag == 0) {
                JOptionPane.showMessageDialog(this, "No Record Found");
            }

            total_stu.setText("" + total_student);
            south_stu.setText("" + south_student);
            north_stu.setText("" + north_student);

            rs.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }
    }//GEN-LAST:event_hall_wiseActionPerformed

    /**
     * show all student's data
     *
     * @param evt
     */
    private void show_stdnt_dataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_stdnt_dataActionPerformed
        // TODO add your handling code here:
        select = 1;
        clr_all_stu_rec(select);
        DefaultTableModel model = (DefaultTableModel) tab1.getModel();
        try {

            Statement state = DB_connect.connection.createStatement();
            String query = "Select * from student";
            ResultSet rs = state.executeQuery(query);
            while (rs.next()) {

                String sname = rs.getString("name");
                String sid = rs.getString("id");
                String sdept = rs.getString("dept");
                String shall = rs.getString("hall");
                model.addRow(new Object[]{sname, sid, sdept, shall});
                total_student++;
                count_all_stu_rec++;
                if (shall.equals("South")) {
                    south_student++;
                } else if (shall.equals("North")) {
                    north_student++;
                }

            }

            total_stu.setText("" + total_student);
            south_stu.setText("" + south_student);
            north_stu.setText("" + north_student);

            rs.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }
    }//GEN-LAST:event_show_stdnt_dataActionPerformed

    /**
     * will show you which students are absent on a particular day
     *
     * @param evt
     */
    private void s1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_s1ActionPerformed
        // TODO add your handling code here:
        /*Date d = partdate.getDate();
         //Date a = d.getTime();
         int day = d.getDay();
         int month = d.getMonth() + 1;
         int year = d.getYear() + 1900;*/
        clr_par();
        int day = Integer.parseInt(d.getSelectedItem().toString());

        int flag = 0;
        //System.out.println(day + " " + month + " " + year);
        DefaultTableModel model = (DefaultTableModel) stab.getModel();
        try {
            Statement s = DB_connect.connection.createStatement();
            String query = "select * from present_student";
            ResultSet rs2 = s.executeQuery(query);
            while (rs2.next()) {
                int stu_id = rs2.getInt("id");
                String stu_pass = rs2.getString("name");
                p_date = rs2.getInt("p_date");
                p_month = rs2.getInt("p_month");
                p_year = rs2.getInt("p_year");
                int fdate = rs2.getInt("from_date");
                int tdate = rs2.getInt("to_date");
                System.out.println(p_date + " " + p_month + " " + p_year + "1");
                if ((day == p_date && month == p_month && year == p_year) || (day >= fdate && day <= tdate)) {

                    model.addRow(new Object[]{stu_id, stu_pass});
                    flag = 1;
                    count_p++;
                }
                t_s.setText("" + count_p);
            }
            if (flag == 0) {
                JOptionPane.showMessageDialog(this, "No Record Found");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }
    }//GEN-LAST:event_s1ActionPerformed

    /**
     * will show you which students are absent during a range of days
     *
     * @param evt
     */
    private void s2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_s2ActionPerformed
        // TODO add your handling code here:
        clr1();
        int fd = Integer.parseInt(fromdate.getSelectedItem().toString());
        //int fm = Integer.parseInt(frommonth.getSelectedItem().toString());
        int td = Integer.parseInt(todate.getSelectedItem().toString());
        //int tm = Integer.parseInt(tomonth.getSelectedItem().toString());
        int flag = 0;
        DefaultTableModel model = (DefaultTableModel) tab.getModel();

        try {
            count = 0;
            Statement s = DB_connect.connection.createStatement();
            String query = "select * from present_student";
            ResultSet rs2 = s.executeQuery(query);
            while (rs2.next()) {
                int stu_id = rs2.getInt("id");
                String stu_pass = rs2.getString("name");
                int fdate = rs2.getInt("from_date");
                int fm = rs2.getInt("from_month");
                int tdate = rs2.getInt("to_date");
                int tm = rs2.getInt("to_month");
                String f1 = Integer.toString(fdate);
                String f2 = String.valueOf(fm);
                String f3 = Integer.toString(fyear);
                String t2 = Integer.toString(tm);
                String t1 = Integer.toString(tdate);
                String t3 = Integer.toString(tyear);
                String cast1 = f1 + "-" + f2 + "-" + f3;
                String cast2 = t1 + "-" + t2 + "-" + t3;
                if ((fd <= fdate && td >= tdate) && (fm >= fmonth && tm <= tmonth)) {

                    model.addRow(new Object[]{stu_id, stu_pass, cast1, cast2});
                    flag = 1;
                    count++;
                }

                t_s.setText("" + count);
            }
            //System.out.println(count);
            if (flag == 0) {
                JOptionPane.showMessageDialog(this, "No Record Found");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }
    }//GEN-LAST:event_s2ActionPerformed

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearActionPerformed
        // TODO add your handling code here:
        clr1();
    }//GEN-LAST:event_clearActionPerformed

    /**
     * all staff data
     *
     * @param evt
     */
    private void showActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showActionPerformed
        // TODO add your handling code here:
        clr2();
        DefaultTableModel model = (DefaultTableModel) tab2.getModel();
        try {

            Statement state = DB_connect.connection.createStatement();
            String query = "Select * from staff";
            ResultSet rs = state.executeQuery(query);
            while (rs.next()) {

                String sid = rs.getString("s_id");
                String sname = rs.getString("s_name");
                model.addRow(new Object[]{sid, sname});
                count1++;
            }
            rs.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }
    }//GEN-LAST:event_showActionPerformed

    /*
     show staff schedule
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        clr3();
        DefaultTableModel model = (DefaultTableModel) tab3.getModel();
        try {

            Statement state = DB_connect.connection.createStatement();
            String s_day = String.valueOf(day.getSelectedItem());
            if (free.isSelected()) {
                String query = "Select * from staff";
                ResultSet rs = state.executeQuery(query);
                int f = 0;
                while (rs.next()) {
                    String sid = rs.getString("s_id");
                    String sname = rs.getString("s_name");
                    int i = rs.getInt("FRI");
                    int j = rs.getInt("SAT_SUN");
                    int k = rs.getInt("MON_TUE");
                    int l = rs.getInt("WED_THU");
                    if (i == 0 && j == 0 && k == 0 && l == 0) {
                        f = 1;
                        model.addRow(new Object[]{sid, sname});
                        count2++;
                    }

                }
                if (f == 0) {
                    JOptionPane.showMessageDialog(this, "No Free Staff");
                }
                rs.close();
            } else {
                if ("Saturday & Sunday".equals(s_day)) {
                    String query = "Select * from staff";
                    ResultSet rs = state.executeQuery(query);
                    int f = 0;
                    while (rs.next()) {

                        String sid = rs.getString("s_id");
                        String sname = rs.getString("s_name");
                        int i = rs.getInt("SAT_SUN");
                        if (i == 1) {
                            f = 1;
                            model.addRow(new Object[]{sid, sname});
                            count2++;
                        }
                    }
                    if (f == 0) {
                        JOptionPane.showMessageDialog(this, "No Staff is selected");
                    }
                    rs.close();
                } else if ("Monday & Tuesday".equals(s_day)) {
                    String query = "Select * from staff";
                    ResultSet rs = state.executeQuery(query);
                    int f = 0;
                    while (rs.next()) {

                        String sid = rs.getString("s_id");
                        String sname = rs.getString("s_name");
                        int i = rs.getInt("MON_TUE");
                        if (i == 1) {
                            f = 1;
                            model.addRow(new Object[]{sid, sname});
                            count2++;
                        }
                    }
                    if (f == 0) {
                        JOptionPane.showMessageDialog(this, "No Staff is selected");
                    }
                    rs.close();
                } else if ("Wednesday & Thursday".equals(s_day)) {
                    String query = "Select * from staff";
                    ResultSet rs = state.executeQuery(query);
                    int f = 0;
                    while (rs.next()) {

                        String sid = rs.getString("s_id");
                        String sname = rs.getString("s_name");
                        int i = rs.getInt("WED_THU");
                        if (i == 1) {
                            f = 1;
                            model.addRow(new Object[]{sid, sname});
                            count2++;
                        }
                    }
                    if (f == 0) {
                        JOptionPane.showMessageDialog(this, "No Staff is selected");
                    }
                    rs.close();
                } else if ("Friday".equals(s_day)) {
                    String query = "Select * from staff";
                    ResultSet rs = state.executeQuery(query);
                    int f = 0;
                    while (rs.next()) {

                        String sid = rs.getString("s_id");
                        String sname = rs.getString("s_name");
                        int i = rs.getInt("FRI");
                        if (i == 1) {
                            f = 1;
                            model.addRow(new Object[]{sid, sname});
                            count2++;
                        }
                    }
                    if (f == 0) {
                        JOptionPane.showMessageDialog(this, "No Staff is selected");
                    }
                    rs.close();
                }
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, "error 2");
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cActionPerformed
        // TODO add your handling code here:
        clr2();
    }//GEN-LAST:event_cActionPerformed

    private void c2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c2ActionPerformed
        // TODO add your handling code here:
        clr3();
    }//GEN-LAST:event_c2ActionPerformed

    private void fc2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fc2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fc2ActionPerformed

    private void fc3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fc3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fc3ActionPerformed

    int f1 = 0;
    int f2 = 0;
    int f3 = 0;
    int id1 = 0;
    int id2 = 0;
    int id3 = 0;
    int satsun1 = 0;
    int satsun2 = 0;
    int satsun3 = 0;

    /**
     * making schedule of staffs for sat sun
     *
     * @param evt
     */
    private void sub1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sub1ActionPerformed
        if (monday == 2) {

            try {
                // TODO add your handling code here:
                //comboList();

                String sname1 = String.valueOf(sc1.getSelectedItem());
                String sname2 = String.valueOf(sc2.getSelectedItem());
                String sname3 = String.valueOf(sc3.getSelectedItem());

                Statement state1 = DB_connect.connection.createStatement();
                String query1 = "select * from staff";
                ResultSet rs1 = state1.executeQuery(query1);

                while (rs1.next()) {
                    String sn = rs1.getString("s_name");
                    if (sn.equals(sname1)) {
                        id1 = rs1.getInt("s_id");
                        f1 = 1;
                    } else if (sn.equals(sname2)) {
                        id2 = rs1.getInt("s_id");
                        f2 = 1;
                    } else if (sn.equals(sname3)) {
                        id3 = rs1.getInt("s_id");
                        f3 = 1;
                    }
                }

                Statement state5 = DB_connect.connection.createStatement();
                String query5 = "select * from again_sched";
                ResultSet rs5 = state1.executeQuery(query5);
                
                while (rs5.next()) {
                    int m = rs5.getInt("S_ID");
                    if (m == id1) {
                        satsun1 = rs5.getInt("SATSUN");
                    } else if (m == id2) {
                        satsun2 = rs5.getInt("SATSUN");
                    } else if (m == id3) {
                        satsun3 = rs5.getInt("SATSUN");
                    }
                }
                
                
                if(satsun1 == 0 && satsun2 == 0 && satsun3 == 0){

                if (sname1 == sname2 || sname2 == sname3 || sname3 == sname1) {
                    JOptionPane.showMessageDialog(this, "Select 3 different Staffs");
                } else {
                    Statement state = DB_connect.connection.createStatement();
                    PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update staff set SAT_SUN =1 where S_NAME = ?  ");
                    preparedStatement.setString(1, sname1);
                    preparedStatement.executeUpdate();
                    PreparedStatement preparedStatement1 = DB_connect.connection.prepareStatement("update staff set SAT_SUN =1 where S_NAME = ?  ");
                    preparedStatement1.setString(1, sname2);
                    preparedStatement1.executeUpdate();
                    PreparedStatement preparedStatement2 = DB_connect.connection.prepareStatement("update staff set SAT_SUN =1 where S_NAME = ?  ");
                    preparedStatement2.setString(1, sname3);
                    preparedStatement2.executeUpdate();
                    JOptionPane.showMessageDialog(this, "Successfully Added");
                    comboList();
                }
                }
                else{
                    JOptionPane.showMessageDialog(this, "You Already Selected Staff For Saturday And Sunday");
                }

                if (f1 == 1) {
                    Statement state2 = DB_connect.connection.createStatement();
                    String query2 = "select * from again_sched";
                    ResultSet rs2 = state1.executeQuery(query2);
                    while (rs2.next()) {
                        int m = rs2.getInt("S_ID");
                        satsun1 = rs2.getInt("SATSUN");
                        if (m == id1 && satsun1 == 0) {
                            satsun1 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set SATSUN = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun1);
                            preparedStatement.setLong(2, (long) id1);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f2 == 1) {
                    Statement state3 = DB_connect.connection.createStatement();
                    String query3 = "select * from again_sched";
                    ResultSet rs3 = state1.executeQuery(query3);
                    while (rs3.next()) {
                        int m = rs3.getInt("S_ID");
                        satsun2 = rs3.getInt("SATSUN");
                        if (m == id2 && satsun2 == 0) {
                            satsun2 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set SATSUN = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun2);
                            preparedStatement.setLong(2, (long) id2);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f3 == 1) {
                    Statement state4 = DB_connect.connection.createStatement();
                    String query4 = "select * from again_sched";
                    ResultSet rs4 = state1.executeQuery(query4);
                    while (rs4.next()) {
                        int m = rs4.getInt("S_ID");
                        satsun3 = rs4.getInt("SATSUN");
                        if (m == id3 && satsun3 == 0) {
                            satsun3 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set SATSUN = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun3);
                            preparedStatement.setLong(2, (long) id3);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

            } catch (SQLException ex) {
                Logger.getLogger(Admin_form.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Make the schedule on Monday");
        }


    }//GEN-LAST:event_sub1ActionPerformed

    /**
     * making schedule of staffs for mon tue
     *
     * @param evt
     */
    private void sub2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sub2ActionPerformed
        // TODO add your handling code here:
        if (monday == 2) {
            try {
                // TODO add your handling code here:
                //comboList();

                String sname1 = String.valueOf(mc1.getSelectedItem());
                String sname2 = String.valueOf(mc2.getSelectedItem());
                String sname3 = String.valueOf(mc3.getSelectedItem());
                
                Statement state1 = DB_connect.connection.createStatement();
                String query1 = "select * from staff";
                ResultSet rs1 = state1.executeQuery(query1);

                while (rs1.next()) {
                    String sn = rs1.getString("s_name");
                    if (sn.equals(sname1)) {
                        id1 = rs1.getInt("s_id");
                        f1 = 1;
                    } else if (sn.equals(sname2)) {
                        id2 = rs1.getInt("s_id");
                        f2 = 1;
                    } else if (sn.equals(sname3)) {
                        id3 = rs1.getInt("s_id");
                        f3 = 1;
                    }
                }

                Statement state5 = DB_connect.connection.createStatement();
                String query5 = "select * from again_sched";
                ResultSet rs5 = state1.executeQuery(query5);
                
                while (rs5.next()) {
                    int m = rs5.getInt("S_ID");
                    if (m == id1) {
                        satsun1 = rs5.getInt("MONTUE");
                    } else if (m == id2) {
                        satsun2 = rs5.getInt("MONTUE");
                    } else if (m == id3) {
                        satsun3 = rs5.getInt("MONTUE");
                    }
                }
                
                if(satsun1 == 0 && satsun2 == 0 && satsun3 == 0){
                
                if (sname1 == sname2 || sname2 == sname3 || sname3 == sname1) {
                    JOptionPane.showMessageDialog(this, "Select 3 different Staffs");
                } else {
                    Statement state = DB_connect.connection.createStatement();
                    PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update staff set MON_TUE =1 where S_NAME = ?  ");
                    preparedStatement.setString(1, sname1);
                    preparedStatement.executeUpdate();
                    PreparedStatement preparedStatement1 = DB_connect.connection.prepareStatement("update staff set MON_TUE =1 where S_NAME = ?  ");
                    preparedStatement1.setString(1, sname2);
                    preparedStatement1.executeUpdate();
                    PreparedStatement preparedStatement2 = DB_connect.connection.prepareStatement("update staff set MON_TUE =1 where S_NAME = ?  ");
                    preparedStatement2.setString(1, sname3);
                    preparedStatement2.executeUpdate();
                    JOptionPane.showMessageDialog(this, "Successfully Added");
                    comboList();
                }
                }
                else{
                    JOptionPane.showMessageDialog(this, "You Already Selected Staff For Monday And Tuesday");
                }
                
                if (f1 == 1) {
                    Statement state2 = DB_connect.connection.createStatement();
                    String query2 = "select * from again_sched";
                    ResultSet rs2 = state1.executeQuery(query2);
                    while (rs2.next()) {
                        int m = rs2.getInt("S_ID");
                        satsun1 = rs2.getInt("MONTUE");
                        if (m == id1 && satsun1 == 0) {
                            satsun1 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set MONTUE = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun1);
                            preparedStatement.setLong(2, (long) id1);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f2 == 1) {
                    Statement state3 = DB_connect.connection.createStatement();
                    String query3 = "select * from again_sched";
                    ResultSet rs3 = state1.executeQuery(query3);
                    while (rs3.next()) {
                        int m = rs3.getInt("S_ID");
                        satsun2 = rs3.getInt("MONTUE");
                        if (m == id2 && satsun2 == 0) {
                            satsun2 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set MONTUE = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun2);
                            preparedStatement.setLong(2, (long) id2);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f3 == 1) {
                    Statement state4 = DB_connect.connection.createStatement();
                    String query4 = "select * from again_sched";
                    ResultSet rs4 = state1.executeQuery(query4);
                    while (rs4.next()) {
                        int m = rs4.getInt("S_ID");
                        satsun3 = rs4.getInt("MONTUE");
                        if (m == id3 && satsun3 == 0) {
                            satsun3 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set MONTUE = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun3);
                            preparedStatement.setLong(2, (long) id3);
                            preparedStatement.executeUpdate();
                        }
                    }
                }
                
                
            } catch (SQLException ex) {
                Logger.getLogger(Admin_form.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Make the schedule on Monday");
        }
    }//GEN-LAST:event_sub2ActionPerformed

    /**
     * making schedule of staffs for wed thurs sun
     *
     * @param evt
     */
    private void sub3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sub3ActionPerformed
        // TODO add your handling code here:
        if (monday == 2) {
            try {
                // TODO add your handling code here:
                //comboList();

                String sname1 = String.valueOf(wc1.getSelectedItem());
                String sname2 = String.valueOf(wc2.getSelectedItem());
                String sname3 = String.valueOf(wc3.getSelectedItem());
                
                Statement state1 = DB_connect.connection.createStatement();
                String query1 = "select * from staff";
                ResultSet rs1 = state1.executeQuery(query1);

                while (rs1.next()) {
                    String sn = rs1.getString("s_name");
                    if (sn.equals(sname1)) {
                        id1 = rs1.getInt("s_id");
                        f1 = 1;
                    } else if (sn.equals(sname2)) {
                        id2 = rs1.getInt("s_id");
                        f2 = 1;
                    } else if (sn.equals(sname3)) {
                        id3 = rs1.getInt("s_id");
                        f3 = 1;
                    }
                }

                Statement state5 = DB_connect.connection.createStatement();
                String query5 = "select * from again_sched";
                ResultSet rs5 = state1.executeQuery(query5);
                
                while (rs5.next()) {
                    int m = rs5.getInt("S_ID");
                    if (m == id1) {
                        satsun1 = rs5.getInt("WEDTHU");
                    } else if (m == id2) {
                        satsun2 = rs5.getInt("WEDTHU");
                    } else if (m == id3) {
                        satsun3 = rs5.getInt("WEDTHU");
                    }
                }
                
                
                
                if(satsun1 == 0 && satsun2 == 0 && satsun3 == 0){
                if (sname1 == sname2 || sname2 == sname3 || sname3 == sname1) {
                    JOptionPane.showMessageDialog(this, "Select 3 different Staffs");
                } else {
                    Statement state = DB_connect.connection.createStatement();
                    PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update staff set WED_THU =1 where S_NAME = ?  ");
                    preparedStatement.setString(1, sname1);
                    preparedStatement.executeUpdate();
                    PreparedStatement preparedStatement1 = DB_connect.connection.prepareStatement("update staff set WED_THU =1 where S_NAME = ?  ");
                    preparedStatement1.setString(1, sname2);
                    preparedStatement1.executeUpdate();
                    PreparedStatement preparedStatement2 = DB_connect.connection.prepareStatement("update staff set WED_THU =1 where S_NAME = ?  ");
                    preparedStatement2.setString(1, sname3);
                    preparedStatement2.executeUpdate();
                    JOptionPane.showMessageDialog(this, "Successfully Added");
                    comboList();
                }
                }
                else{
                    JOptionPane.showMessageDialog(this, "You Already Selected Staff For Wedesday And Thursday");
                }
                
                if (f1 == 1) {
                    Statement state2 = DB_connect.connection.createStatement();
                    String query2 = "select * from again_sched";
                    ResultSet rs2 = state1.executeQuery(query2);
                    while (rs2.next()) {
                        int m = rs2.getInt("S_ID");
                        satsun1 = rs2.getInt("WEDTHU");
                        if (m == id1 && satsun1 == 0) {
                            satsun1 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set WEDTHU = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun1);
                            preparedStatement.setLong(2, (long) id1);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f2 == 1) {
                    Statement state3 = DB_connect.connection.createStatement();
                    String query3 = "select * from again_sched";
                    ResultSet rs3 = state1.executeQuery(query3);
                    while (rs3.next()) {
                        int m = rs3.getInt("S_ID");
                        satsun2 = rs3.getInt("WEDTHU");
                        if (m == id2 && satsun2 == 0) {
                            satsun2 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set WEDTHU = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun2);
                            preparedStatement.setLong(2, (long) id2);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f3 == 1) {
                    Statement state4 = DB_connect.connection.createStatement();
                    String query4 = "select * from again_sched";
                    ResultSet rs4 = state1.executeQuery(query4);
                    while (rs4.next()) {
                        int m = rs4.getInt("S_ID");
                        satsun3 = rs4.getInt("WEDTHU");
                        if (m == id3 && satsun3 == 0) {
                            satsun3 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set WEDTHU = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun3);
                            preparedStatement.setLong(2, (long) id3);
                            preparedStatement.executeUpdate();
                        }
                    }
                }
                
                
            } catch (SQLException ex) {
                Logger.getLogger(Admin_form.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Make the schedule on Monday");
        }
    }//GEN-LAST:event_sub3ActionPerformed

    /**
     * making schedule of staffs for fri
     *
     * @param evt
     */
    private void sub4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sub4ActionPerformed
        // TODO add your handling code here:
        if (monday == 2) {
            try {
                // TODO add your handling code here:
                //comboList();
//
                String sname1 = String.valueOf(fc1.getSelectedItem());
                String sname2 = String.valueOf(fc2.getSelectedItem());
                String sname3 = String.valueOf(fc3.getSelectedItem());
                
                Statement state1 = DB_connect.connection.createStatement();
                String query1 = "select * from staff";
                ResultSet rs1 = state1.executeQuery(query1);

                while (rs1.next()) {
                    String sn = rs1.getString("s_name");
                    if (sn.equals(sname1)) {
                        id1 = rs1.getInt("s_id");
                        f1 = 1;
                    } else if (sn.equals(sname2)) {
                        id2 = rs1.getInt("s_id");
                        f2 = 1;
                    } else if (sn.equals(sname3)) {
                        id3 = rs1.getInt("s_id");
                        f3 = 1;
                    }
                }

                Statement state5 = DB_connect.connection.createStatement();
                String query5 = "select * from again_sched";
                ResultSet rs5 = state1.executeQuery(query5);
                
                while (rs5.next()) {
                    int m = rs5.getInt("S_ID");
                    if (m == id1) {
                        satsun1 = rs5.getInt("FRI");
                    } else if (m == id2) {
                        satsun2 = rs5.getInt("FRI");
                    } else if (m == id3) {
                        satsun3 = rs5.getInt("FRI");
                    }
                }
                
                
                if(satsun1 == 0 && satsun2 == 0 && satsun3 == 0){
                if (sname1 == sname2 || sname2 == sname3 || sname3 == sname1) {
                    JOptionPane.showMessageDialog(this, "Select 3 different Staffs");
                } else {
                    Statement state = DB_connect.connection.createStatement();
                    PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update staff set FRI =1 where S_NAME = ?  ");
                    preparedStatement.setString(1, sname1);
                    preparedStatement.executeUpdate();
                    PreparedStatement preparedStatement1 = DB_connect.connection.prepareStatement("update staff set FRI =1 where S_NAME = ?  ");
                    preparedStatement1.setString(1, sname2);
                    preparedStatement1.executeUpdate();
                    PreparedStatement preparedStatement2 = DB_connect.connection.prepareStatement("update staff set FRI =1 where S_NAME = ?  ");
                    preparedStatement2.setString(1, sname3);
                    preparedStatement2.executeUpdate();
                    JOptionPane.showMessageDialog(this, "Successfully Added");
                    comboList();
                }
                }
                else{
                    JOptionPane.showMessageDialog(this, "You Already Selected Staff For Friday");
                }
                
                if (f1 == 1) {
                    Statement state2 = DB_connect.connection.createStatement();
                    String query2 = "select * from again_sched";
                    ResultSet rs2 = state1.executeQuery(query2);
                    while (rs2.next()) {
                        int m = rs2.getInt("S_ID");
                        satsun1 = rs2.getInt("FRI");
                        if (m == id1 && satsun1 == 0) {
                            satsun1 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set FRI = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun1);
                            preparedStatement.setLong(2, (long) id1);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f2 == 1) {
                    Statement state3 = DB_connect.connection.createStatement();
                    String query3 = "select * from again_sched";
                    ResultSet rs3 = state1.executeQuery(query3);
                    while (rs3.next()) {
                        int m = rs3.getInt("S_ID");
                        satsun2 = rs3.getInt("FRI");
                        if (m == id2 && satsun2 == 0) {
                            satsun2 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set FRI = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun2);
                            preparedStatement.setLong(2, (long) id2);
                            preparedStatement.executeUpdate();
                        }
                    }
                }

                if (f3 == 1) {
                    Statement state4 = DB_connect.connection.createStatement();
                    String query4 = "select * from again_sched";
                    ResultSet rs4 = state1.executeQuery(query4);
                    while (rs4.next()) {
                        int m = rs4.getInt("S_ID");
                        satsun3 = rs4.getInt("FRI");
                        if (m == id3 && satsun3 == 0) {
                            satsun3 = 1;
                            PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("update again_sched set FRI = ? where S_ID = ?");
                            preparedStatement.setLong(1, (long) satsun3);
                            preparedStatement.setLong(2, (long) id3);
                            preparedStatement.executeUpdate();
                        }
                    }
                }
                
                
                
            } catch (SQLException ex) {
                Logger.getLogger(Admin_form.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Make the schedule on Monday");
        }
    }//GEN-LAST:event_sub4ActionPerformed

    int count_sel = 0;

    /**
     * will show you the selected menu
     *
     * @param evt
     */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:

        clr_sel();
        count_sel = 0;
        DefaultTableModel model = (DefaultTableModel) tabs.getModel();

        String d = String.valueOf(sday.getSelectedItem());
        if (d == "Monday") {
            d = "Mon";
        } else if (d == "Tuesday") {
            d = "Tues";
        } else if (d == "Wednesday") {
            d = "Wed";
        } else if (d == "Thursday") {
            d = "Thus";
        } else if (d == "Friday") {
            d = "fri";
        } else if (d == "Saturday") {
            d = "Sat";
        } else if (d == "Sunday") {
            d = "Sun";
        }
        Statement state;
        int max1 = -1;
        int max2 = -1;
        int max3 = -1;
        int max4 = -1;
        int fid1 = -1;
        int fid2 = -1;
        int fid3 = -1;
        int fid4 = -1;
        String fname1 = "";
        String fname2 = "";
        String fname3 = "";
        String fname4 = "";
        try {
            state = DB_connect.connection.createStatement();
            //String query1 = "Select id from students";
            //ResultSet rs1 = state.executeQuery(query1);
            //PreparedStatement preparedStatement = DB_connect.connection.prepareStatement("select * from food where DAY = ?");
            //preparedStatement.setString(1, d);
            System.out.println(d);
            //preparedStatement.executeQuery();
            String query = "select * from food";
            ResultSet rs1 = state.executeQuery(query);
            //System.out.println("abcd");

            while (rs1.next()) {
                int v = rs1.getInt("VOTE");
                int id = rs1.getInt("F_ID");
                String day = rs1.getString("CATEGORY");
                String d1 = rs1.getString("DAY");
                //System.out.println(v + id + day);
                if (v >= max1 && day.equals("Launch") && d1.equals(d)) {
                    max1 = v;
                    fid1 = id;
                    fname1 = rs1.getString("F_NAME");
                }
            }
            String query2 = "select * from food";
            ResultSet rs2 = state.executeQuery(query2);
            while (rs2.next()) {
                int v = rs2.getInt("VOTE");
                int id = rs2.getInt("F_ID");
                String day = rs2.getString("CATEGORY");
                String d1 = rs2.getString("DAY");
                //System.out.println(v + id + day);
                if (v >= max2 && day.equals("Launch") && id != fid1 && d1.equals(d)) {
                    max2 = v;
                    fid2 = id;
                    fname2 = rs2.getString("F_NAME");
                }
            }

            String query3 = "select * from food";
            ResultSet rs3 = state.executeQuery(query3);
            while (rs3.next()) {
                int v = rs3.getInt("VOTE");
                int id = rs3.getInt("F_ID");
                String day = rs3.getString("CATEGORY");
                String d1 = rs3.getString("DAY");
                //System.out.println(v + id + day);
                if (v >= max3 && day.equals("Dinner") && d1.equals(d)) {
                    max3 = v;
                    fid3 = id;
                    fname3 = rs3.getString("F_NAME");
                }
            }
            String query4 = "select * from food";
            ResultSet rs4 = state.executeQuery(query4);
            while (rs4.next()) {
                int v = rs4.getInt("VOTE");
                int id = rs4.getInt("F_ID");
                String day = rs4.getString("CATEGORY");
                String d1 = rs4.getString("DAY");
                //System.out.println(v + id + day);
                if (v >= max4 && day.equals("Launch") && id != fid3 && d1.equals(d)) {
                    max4 = v;
                    fid4 = id;
                    fname4 = rs4.getString("F_NAME");
                }
            }
            model.addRow(new Object[]{fname1, fname3});
            count_sel++;
            model.addRow(new Object[]{fname2, fname4});
            count_sel++;

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "error 2");
            ex.printStackTrace();
            //Logger.getLogger(Admin_form.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_jButton4ActionPerformed

    private void refActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refActionPerformed
        // TODO add your handling code here:
        clr_sel();
        count_sel = 0;
    }//GEN-LAST:event_refActionPerformed

    private void south_stuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_south_stuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_south_stuActionPerformed

    private void clear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clear1ActionPerformed
        // TODO add your handling code here:
        clr_par();

    }//GEN-LAST:event_clear1ActionPerformed

    private void refresh_all_foodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refresh_all_foodActionPerformed
        // TODO add your handling code here:
        clr_all_food();
    }//GEN-LAST:event_refresh_all_foodActionPerformed

    private void clr_all_stu_recActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clr_all_stu_recActionPerformed
        // TODO add your handling code here:
        if (select == 1) {
            clr_all_stu_rec(select);
        } else if (select == 2) {
            clr_all_stu_rec(2);
        }
    }//GEN-LAST:event_clr_all_stu_recActionPerformed

    public void clr_all_stu_rec(int j) {
        if (j == 1) {
            for (int i = 0; i < count_all_stu_rec; i++) {
                tab_clr_all_stu_rec(0);
            }
            count_all_stu_rec = 0;
            total_student = 0;
            south_student = 0;
            north_student = 0;
            total_stu.setText("" + count_all_stu_rec);
            south_stu.setText("" + total_student);
            north_stu.setText("" + north_student);
        } else if (j == 2) {
            for (int i = 0; i < count_par_hall; i++) {
                tab_clr_all_stu_rec(0);
            }
            count_par_hall = 0;
            total_student = 0;
            south_student = 0;
            north_student = 0;
            total_stu.setText("" + total_student);
            south_stu.setText("" + south_student);
            north_stu.setText("" + north_student);
        }

    }

    public void clr_all_food() {
        for (int i = 0; i < count_all_food; i++) {
            tab_clr_all_food(0);
        }
        count_all_food = 0;
    }

    public void clr_par() {
        for (int i = 0; i < count_p; i++) {
            tab_clr_par(0);
        }
        count_p = 0;
        t_s.setText("" + count_p);
    }

    public void clr_sel() {
        for (int i = 0; i < count_sel; i++) {
            tab_clr_sel(0);
        }
        t_s.setText("" + count_sel);
    }

    public void clr1() {
        for (int i = 0; i < count; i++) {
            tab_clr1(0);
        }
        count = 0;
        t_s.setText("" + count);
    }

    public void clr2() {
        for (int i = 0; i < count1; i++) {
            tab_clr2(0);
        }
        count1 = 0;
        //t_s.setText("" + count1);
    }

    public void clr3() {
        for (int i = 0; i < count2; i++) {
            tab_clr3(0);
        }
        count2 = 0;
        //t_s.setText("" + count1);
    }

    public void tab_clr1(int i) {
        DefaultTableModel model = (DefaultTableModel) tab.getModel();
        model.removeRow(i);
    }

    public void tab_clr_all_food(int i) {
        DefaultTableModel model_all_food = (DefaultTableModel) allfood.getModel();
        model_all_food.removeRow(i);
    }

    public void tab_clr_sel(int i) {
        DefaultTableModel model = (DefaultTableModel) tabs.getModel();
        model.removeRow(i);
    }

    public void tab_clr2(int i) {
        DefaultTableModel model1 = (DefaultTableModel) tab2.getModel();
        model1.removeRow(i);
    }

    public void tab_clr3(int i) {
        DefaultTableModel model2 = (DefaultTableModel) tab3.getModel();
        model2.removeRow(i);
    }

    public void tab_clr_par(int i) {
        DefaultTableModel model3 = (DefaultTableModel) stab.getModel();
        model3.removeRow(i);
    }

    public void tab_clr_all_stu_rec(int i) {
        DefaultTableModel model_all_stu_rec = (DefaultTableModel) tab1.getModel();
        model_all_stu_rec.removeRow(i);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin_form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin_form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin_form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin_form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Admin_form().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable allfood;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton c;
    private javax.swing.JButton c2;
    private javax.swing.JButton clear;
    private javax.swing.JButton clear1;
    private javax.swing.JButton clr_all_stu_rec;
    private javax.swing.JLabel cmon;
    private javax.swing.JLabel cweek;
    private javax.swing.JComboBox d;
    private javax.swing.JLabel date;
    private javax.swing.JComboBox day;
    private javax.swing.JComboBox fc1;
    private javax.swing.JComboBox fc2;
    private javax.swing.JComboBox fc3;
    private javax.swing.JRadioButton free;
    private javax.swing.JComboBox fromdate;
    private javax.swing.JTextField frommonth;
    private javax.swing.JTextField fromyear;
    private javax.swing.JButton hall_wise;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JMenuItem logOff;
    private javax.swing.JTextField m;
    private javax.swing.JComboBox mc1;
    private javax.swing.JComboBox mc2;
    private javax.swing.JComboBox mc3;
    private javax.swing.JLabel men;
    private javax.swing.JLabel mond;
    private javax.swing.JTextField north_stu;
    private javax.swing.JButton ref;
    private javax.swing.JButton refresh_all_food;
    private javax.swing.JButton s1;
    private javax.swing.JButton s2;
    private javax.swing.JButton s_f;
    private javax.swing.JComboBox sc1;
    private javax.swing.JComboBox sc2;
    private javax.swing.JComboBox sc3;
    private javax.swing.JComboBox sday;
    private javax.swing.JButton show;
    private javax.swing.JButton show_stdnt_data;
    private javax.swing.JTextField south_stu;
    private javax.swing.JTable stab;
    private javax.swing.JComboBox stuhall;
    private javax.swing.JButton sub1;
    private javax.swing.JButton sub2;
    private javax.swing.JButton sub3;
    private javax.swing.JButton sub4;
    private javax.swing.JTextField t_s;
    private javax.swing.JTable tab;
    private javax.swing.JTable tab1;
    private javax.swing.JTable tab2;
    private javax.swing.JTable tab3;
    private javax.swing.JTable tabs;
    private javax.swing.JLabel time;
    private javax.swing.JComboBox todate;
    private javax.swing.JTextField tomonth;
    private javax.swing.JTextField total_stu;
    private javax.swing.JTextField toyear;
    private javax.swing.JComboBox wc1;
    private javax.swing.JComboBox wc2;
    private javax.swing.JComboBox wc3;
    private javax.swing.JTextField y;
    // End of variables declaration//GEN-END:variables
    int total_student = 0;
    int south_student = 0;
    int north_student = 0;
    int p_date;
    int p_month;
    int p_year;
    int fmonth;
    int tmonth;
    int fyear;
    int tyear;
    int count = 0;
    int count1 = 0;
    int count2 = 0;
    int count_p = 0;
    int count_all_food = 0;
    int count_all_stu_rec = 0;
    int select = 0;
    int count_par_hall = 0;
}
